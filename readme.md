# Device Registry API v1

## Table of Contents
<!-- TOC -->

- [Table of Contents](#markdown-header-table-of-contents)
- [Capabilities](#markdown-header-capabilities)
    - [Device Identity Administration](#markdown-header-device-identity-administration)
    - [Device Authentication and Token Service](#markdown-header-device-authentication-and-token-service)
- [Accessing the API](#markdown-header-accessing-the-api)
    - [Device Administration Base URLs](#markdown-header-device-administration-base-urls)
    - [Device Authentication URLs](#markdown-header-device-authentication-urls)
- [Authorization](#markdown-header-authorization)
    - [Everyone](#markdown-header-everyone)
    - [Global Support](#markdown-header-global-support)
    - [Model Administrators](#markdown-header-model-administrators)
    - [System Administrators](#markdown-header-system-administrators)
    - [Authorization Matrix](#markdown-header-authorization-matrix)
- [Conventions](#markdown-header-conventions)
- [Errors](#markdown-header-errors)
    - [Types](#markdown-header-types)
    - [Representation](#markdown-header-representation)
- [Token](#markdown-header-token)
    - [Representation](#markdown-header-representation_1)
    - [Methods](#markdown-header-methods)
        - [Authenticate with Certificate (POST)](#markdown-header-authenticate-with-certificate-post)
        - [Authenticate with Credentials (POST)](#markdown-header-authenticate-with-credentials-post)
- [Model](#markdown-header-model)
    - [Representation](#markdown-header-representation_2)
    - [Methods](#markdown-header-methods_1)
        - [List Models (GET)](#markdown-header-list-models-get)
        - [Single Model (GET)](#markdown-header-single-model-get)
        - [Create Model (POST)](#markdown-header-create-model-post)
        - [Update Model (PUT)](#markdown-header-update-model-put)
        - [Remove Model (DELETE)](#markdown-header-remove-model-delete)
- [Model Manufacturer](#markdown-header-model-manufacturer)
    - [Representation](#markdown-header-representation_3)
    - [Methods](#markdown-header-methods_2)
        - [List Manaufacturers (GET)](#markdown-header-list-manaufacturers-get)
- [Model Propery](#markdown-header-model-propery)
    - [Representation](#markdown-header-representation_4)
    - [Methods](#markdown-header-methods_3)
        - [Add Model Property (POST)](#markdown-header-add-model-property-post)
        - [Update Model Property (PUT)](#markdown-header-update-model-property-put)
        - [Remove Model Property (DELETE)](#markdown-header-remove-model-property-delete)
- [Model Device Property Template](#markdown-header-model-device-property-template)
    - [Representation](#markdown-header-representation_5)
    - [Methods](#markdown-header-methods_4)
        - [Add Device Property Template (POST)](#markdown-header-add-device-property-template-post)
        - [Update Device Property Template (PUT)](#markdown-header-update-device-property-template-put)
        - [Remove Device Property Template (DELETE)](#markdown-header-remove-device-property-template-delete)
- [Device](#markdown-header-device)
- [Device Status](#markdown-header-device-status)
- [Device Property](#markdown-header-device-property)
- [Resource Name](#markdown-header-resource-name)
    - [Representation](#markdown-header-representation_6)
    - [Methods](#markdown-header-methods_5)
        - [List Resources (GET)](#markdown-header-list-resources-get)
        - [Single Resource (GET)](#markdown-header-single-resource-get)
        - [Create Resource (POST)](#markdown-header-create-resource-post)
        - [Update Resource (PUT)](#markdown-header-update-resource-put)
        - [Remove Resource (DELETE)](#markdown-header-remove-resource-delete)

<!-- /TOC -->

---

## Capabilities
[Top](#markdown-header-table-of-contents)

### Device Identity Administration
 Register devices will be assigned a globally unique identifier that can be referenced by applications and APIs. Device Registry is not responsible for determining the ownership of a device or what applications or entities have authorization to a device's collected data (data visibility).

### Device Authentication and Token Service
Registered devices can authenticate and receive TPaaS tokens that can be used to invoke APIs that are managed by TPaaS API Manager.

---

## Accessing the API
[Top](#markdown-header-table-of-contents)

All Device Administration APIs are protected by the TPaaS API Manager and require a TPaaS Identity Server generated bearer token to invoke. This token must be put in the Authorization header of the request.

Device Authentication APIs are not protected by the TPaaS API Manager, but for a device to successfully authenticate with TPaaS Identity Server, it requires a client id and client secret provided by the TPaaS API Manager.

### Device Administration Base URLs
* Production: https://api.trimble.com/t/deviceregistry/1.0.0
* Staging: https://api-stg.trimble.com/t/deviceregistry/1.0.0

### Device Authentication URLs
* Production: [TODO]
* Staging: [TODO]

---

## Authorization
[Top](#markdown-header-table-of-contents)

### Everyone
All consumers have the ability to read/query/search the Device Administration APIs to obtain general information regarding models and devices.

### Global Support
In order to assist customers in troubleshooting issues, they will have elevated privileges to read protected information regarding device authentication and the permission to unlock or modify the status of existing devices. They will not have permission to create/update/delete device identities.

### Model Administrators
Responsible for updating, and deleting models and devices. Model Administrators will be granted this permission by;
* Creating a Model
* System Administrators and Model Administrators.

Once a Model is created, the creator is established as the Model Administrator. Model Administrators and System Administrators will have the ability to update/delete that model and create/update/delete devices of that model.

### System Administrators
Responsible for maintaining the Device Registry system overall. The System Administrators can perform any action in the system.

### Authorization Matrix
|Endpoint|Everyone|Global Support|Model Admin|System Admin|
|---|:---:|:---:|:---:|:---:|
|GET /models|x|x|x|x|
|GET /models/{code}|x|x|x|x|
|POST /models|x|x|x|x|
|PUT /models/{code}|||**|x|
|DELETE /models/{code}|||**|x|
|GET /models/{code}|||**|x|
|GET /models/{code}|||**|x|
|GET /models/manufacturers|x|x|x|x|
|POST /models/{code}/properties|||**|x|
|PUT /models/{code}/properties/{key}|||**|x|
|DELETE /models/{code}/properties/{key}|||**|x|
|POST /models/{code}/devicePropertyTemplates|||**|x|
|PUT /models/{code}/devicePropertyTemplates/{key}|||**|x|
|DELETE /models/{code}/devicePropertyTemplates/{key}|||**|x|
|GET /devices|x|x|x|x|
|GET /devices/{id}|x|x|x|x|
|POST /devices|||**|x|
|PUT /devices/{id}|||**|x|
|DELETE /devices/{id}|||**|x|
|GET /devices|x|x|x|x|
|GET /devices/{id}|x|x|x|x|
|GET /devices/{id}/status||x|**|x|
|POST /devices/{id}/properties|||**|x|
|PUT /devices/{id}/properties/{key}|||**|x|
|DELETE /devices/{id}/properties/{key}|||**|x|
** Model Admin can only perform this activity on a specific model and devices derived from that model.
---

## Conventions

[UPDATE LINK] Refer to [API General Convention Guidelines](http://www.google.com)

* All properties are _Required_ unless explicitly stated as _Optional_.
* All successful response status codes are `200 OK` unless eplicitly stated otherwise. 
* All Dates, Times, and Durations are in `ISO 8601` format with UTC time zone designation unless explicitly stated otherwise. Ex.: `2017-02-14T11:55:07Z`

## Errors
[Top](#markdown-header-table-of-contents)

In the event the API is unable to successfully process a request, the response should be an error. The following errors will be sent back by the API.

[UPDATE LINK] Refer to [API Errors Guidelines](http://www.google.com)

### Types

|Type|Status|Description|
|---|---|---|
|**Bad Request**|400|Parameters of the request did not meet requirements. Modify the request and try again.|
|**Unauthorized**|401|Unable to authenticate with the provided request parameters. Modify the request and try again.|
|**Forbidden**|403|Attempting to perform an action that is not permitted by the current authenticated user.|
|**Not Found**|404|The requested resource was not located. Modify the request and try again.|
|**Internal Server Exception**, and all 500 level errors|500 (+)|The API encountered an unexpected event during processing. Attempt the request again, if the problem persists, please contact Support.|

### Representation

```json
{
  "status": number,
  "timeStamp": string,
  "message": string,
  "moreInfo": string,
  "errors": [
    {
      "property": string,
      "attemptedValue": string,
      "message": string
    }
  ]
}
```

|Name|Type|Description|
|---|---|---|
|**status**|string|HTTP status code of error.|
|**timeStamp**|string|Date and time of the error.|
|**message**|string|Descriptive message of the error.|
|**moreInfo**|string|Provided if more troubleshooting information is available.|
|**errors**|array|Array of error details. Available when the request parameters fail validation.|
|errors[].**property**|string|Parameter that failed validation.|
|errors[].**attemptedValue**|string|Attempted value for the parameter of the request.|
|errors[].**message**|string|Descriptive message of how the parameter failed.|

---

## Token
[Top](#markdown-header-table-of-contents)

TPaaS Identity tokens used to access APIs managed by TPaaS API Manager.

### Representation

```json
{
    "scope": string,
    "token_type": string,
    "expires_in": number,
    "access_token": string
}
```
|Name|Type|Description|
|---|---|---|
|**scope**|string|The device's identity (UUID) prefixed by "DID_".|
|**token_type**|string|"bearer"|
|**expires_in**|number|Seconds until the token expires. Current default is 50400 (14 hours).|
|**access_token**|strng|Token used to access TPaaS API Manager protected resources.|

### Methods
[Top](#markdown-header-table-of-contents)

* [Authenticate with Certificate (POST)](#markdown-header-authenticate-with-certificate-post)
* [Authenticate with Credentials (POST)](#markdown-header-authenticate-with-credentials-post)

---

#### Authenticate with Certificate (POST)
[Top](#markdown-header-table-of-contents)

Invoking this endpoint requires the client to have a X.509 certificate configured for use in TLS mutual authentication where the server will require client-side authentication. The certificate chain will have to be validated, requiring a CA certificate (in the case of self signing) that the client certificate is derived from to be trusted on the server.

_**Request**_

```
POST /certificates/token
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Headers**_|
|**Authorization**|string|Base64 encoded client Id and secret separated by a colon with scheme as “Basic”. Ex: "Basic Y2xpZW50SWQ6c2VjcmV0". When the literal string “clientId:secret” is Base64 encoded it becomes Y2xpZW50SWQ6c2VjcmV0|

_**Request Body**_

There is no request body.

_**Response**_

If successful, this method will return a token resource.

---

#### Authenticate with Credentials (POST)
[Top](#markdown-header-table-of-contents)

[TODO]

_**Request**_

```
POST /credentials/token
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Headers**_|
|**Authorization**|string|Base64 encoded client Id and secret separated by a colon with scheme as “Basic”. Ex: "Basic Y2xpZW50SWQ6c2VjcmV0"|

_**Request Body**_

```json
{
    "deviceName": string,
    "password": string
}
```

_**Response**_

If successful, this method will return a token resource.

---

## Model
[Top](#markdown-header-table-of-contents)

Represents a type or class of device. Creating a device of a certain model allows the device to inherit properties of that model.

### Representation

```json
{
  "code": string,
  "name": string,
  "manufacturer": string,
  "deviceKey": string,
  "deviceKeyPattern": string,
  "deviceNamePattern": string,
  "properties": [
    {
      "key": string,
      "value": object
    }
  ],
  "devicePropertyTemplates": [
    {
      "key": string,
      "defaultValue": object
    }
  ],
  "createdUtc": string,
  "updatedUtc": string
}
```
|Name|Type|Description|
|---|---|---|
|**code**|string|Identifies the model. Ex. DCM300, SNM900, etc|
|**name**|string|Name of the model.|
|**manufacturer**|string|Name of the manufacturer.|
|**deviceKey**|string|Identifier type used to identify devices of this model type. Ex. SerialNumber, IMEI, MAC, etc.|
|**deviceKeyPattern**|string|Regex pattern used to validate the value give for a device's key during device create and update operations.|
|**deviceNamePattern**|string|Regex pattern used to validate the value of the device's name.|
|**properties**|array|Array of model properties.|
|properties[].**key**|string|Key of the model property.|
|properties[].**value**|object|Value of the model property.|
|**devicePropertyTemplates**|array|Array of the device property templates. These will be copied to a device of this model type upon creation.|
|devicePropertyTemplates[].**key**|string|Key of the device property template.|
|devicePropertyTemplates[].**defaultValue**|object|Value of the device property template.|
|**createdUtc**|string|Date and time the model was created.|
|**updatedUtc**|string|Date and time the model was last updated.|


### Methods
[Top](#markdown-header-table-of-contents)

* [List Models (GET)](#markdown-header-list-models-get) - returns a paginated list of models.
* [Single Model (GET)](#markdown-header-single-model-get) - returns a single model.
* [Create Model (POST)](#markdown-header-create-model-post) - creates a single model.
* [Update Model (PUT)](#markdown-header-update-model-put) - updates a single model.
* [Remove Model (DELETE)](#markdown-header-remove-model-delete) - deletes a single model.

---

#### List Models (GET)
[Top](#markdown-header-table-of-contents)

Returns a paginated list of model resources.

_**Request**_

```
GET /models
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Querystring**_|
|**page**|number|Indicates which page of results to return. Default is 1. _Optional_|
|**pagesize**|number|Maximum number of items returned in one result page. Default is 10. Maximum is 100. _Optional_|
|**sort**|string|The order of the models returned in the result. Multiple sorts are supported. The default is "Code". Acceptable values are: "Code", "Name", "Manufacturer". _Optional_|
|**{sort}.dir**|string|Direction of the sort. Default is "Asc". To apply an ascending order when sort=Code, specify: Code.dir=Asc. _Optional_|
|**q**|string|Free text search (starts with, ends with, contains) within Code, Name, and Manufacturer. Ignored when both q and manufacturer are provided. _Optional_|
|**manufacturer**|string|Search by exact Manufacturer name. _Optional_|
__

_**Request Body**_

There is no request body.

_**Response**_

If successful, this method will return a response body with the following structure.

```json
{
  "page": number,
  "pageSize": number,
  "sort": [
    {
      "property": string,
      "direction": string
    }
  ],
  "itemCount": number,
  "totalPages": number,
  "totalItems": number,
  "items": [
  	model resource
  ]
}
```

---

#### Single Model (GET)
[Top](#markdown-header-table-of-contents)

Returns a single model resource.

_**Request**_

```
GET /models/{id}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|The identifier of the model.|

_**Request Body**_

There is no request body.

_**Response**_

If successful, this method will return a model resource.

---

#### Create Model (POST)
[Top](#markdown-header-table-of-contents)

Description of creating a resource...

_**Request**_

```
POST /resources/
```

_**Parameters**_

There are no request parameters.

_**Request Body**_

```json
{
  "code": string,
  "name": string,
  "manufacturer": string,
  "deviceKey": string,
  "deviceKeyPattern": string,
  "deviceNamePattern": string
}
```

|Name|Type|Description|
|---|---|---|
|**code**|string|Identifies the model. Required to be unique within the system.|
|**name**|string|Name of the model.|
|**manufacturer**|string|Name of the manufacturer.|
|**deviceKey**|string|Identifier type used to identify devices of this model type. Ex. SerialNumber, IMEI, MAC, etc.|
|**deviceKeyPattern**|string|Regex pattern used to validate the value give for a device's key during device create and update operations.|
|**deviceNamePattern**|string|Regex pattern used to validate the value of the device's name.|

_**Response**_

If successful, this method returns a status of 201 Created and a response body containg the newly created model code.

```json
"code of model"
```

---

#### Update Model (PUT)
[Top](#markdown-header-table-of-contents)

Updates a single resource.

_**Request**_

```
PUT /models/{code}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|Code identifying of the model|

_**Request Body**_

```json
{
  "name": string,
  "manufacturer": string,
  "deviceKey": string,
  "deviceKeyPattern": string,
  "deviceNamePattern": string
}
```

|Name|Type|Description|
|---|---|---|
|**name**|string|Name of the model. _Optional_|
|**manufacturer**|string|Name of the manufacturer. _Optional_|
|**deviceKey**|string|Identifier type used to identify devices of this model type. Ex. SerialNumber, IMEI, MAC, etc. _Optional_|
|**deviceKeyPattern**|string|Regex pattern used to validate the value give for a device's key during device create and update operations. _Optional_|
|**deviceNamePattern**|string|Regex pattern used to validate the value of the device's name. _Optional_|

_**Response**_

If successful, this method returns an empty response body.

---

#### Remove Model (DELETE)
[Top](#markdown-header-table-of-contents)

Deletes a single model resource. There must not be any existing devices for the model being deleted.

_**Request**_

```
DELETE /models/{code}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|The identifier of the model.|

_**Request Body**_

There is no request body.

_**Response**_

If successful, will not return a response body.

## Model Manufacturer
[Top](#markdown-header-table-of-contents)

The manufacturer of a model.

### Representation

```json
string
```

### Methods
[Top](#markdown-header-table-of-contents)

* [List Manaufacturers (GET)](#markdown-header-list-manufacturers-get) - returns a paginated list of manufacturers.
* [Single Manaufacturer (GET)](#markdown-header-single-manufacturer-get) - returns a single manufacturer.
* [Create Manaufacturer (POST)](#markdown-header-create-manufacturer-post) - creates a single manufacturer.
* [Update Manaufacturer (PUT)](#markdown-header-update-manufacturer-put) - updates a single manufacturer.
* [Remove Manaufacturer (DELETE)](#markdown-header-remove-manufacturer-delete) - deletes a single manufacturer.

---

#### List Manaufacturers (GET)
[Top](#markdown-header-table-of-contents)

Returns a paginated list of manufacturers.

_**Request**_

```
GET /models/manufacturers
```

_**Parameters**_

There are no parameters.

_**Request Body**_

There is no request body.

_**Response**_

If successful, this method will return a response body with the following structure.

```json
{
  "page": number,
  "pageSize": number,
  "sort": [
  ],
  "itemCount": number,
  "totalPages": number,
  "totalItems": number,
  "items": [
  	model manufacturer resource
  ]
}
```

---

## Model Propery
[Top](#markdown-header-table-of-contents)

A property of a model in key/value form.

### Representation

```json
{
    "key": string,
    "value": object
}
```
|Name|Type|Description|
|---|---|---|
|**key**|string|Identifies model property.|
|**value**|object|Value of model property.|

### Methods
[Top](#markdown-header-table-of-contents)

* [Add Model Property (POST)](#markdown-header-add-model-property-post) - adds a single model property to a model’s property collection.
* [Update Model Property (PUT)](#markdown-header-update-model-property-put) - updates a single model property in a model’s property collection.
* [Remove Model Property (DELETE)](#markdown-header-remove-model-property-delete) - removes a single model property from a model’s property collection.

---
#### Add Model Property (POST)
[Top](#markdown-header-table-of-contents)

Adds a single model property to the model’s property collection. CAUTION: Invoking this API with an existing key will overwrite that property’s value, updating it. TODO: CHANGE IT SO THIS DOESN'T HAPPEN.

_**Request**_

```
POST /models/{code}/properties
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|Code identifying the model.|

_**Request Body**_

```json
{
  "key": string,
  "value": object
}
```

|Name|Type|Description|
|---|---|---|
|**key**|string|Identifies the model property.|
|**value**|object|Value of the property. Accepted values are: string, number, and boolean.|

_**Response**_

If successful, this method returns an empty response body.

---

#### Update Model Property (PUT)
[Top](#markdown-header-table-of-contents)

Updates a single model property in the model’s property collection.

_**Request**_

```
PUT /models/{code}/properties/{key}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|Code identifying the model.|
|**key**|string|Key of the model's property.|

_**Request Body**_

```json
{
    "value": object
}
```

|Name|Type|Description|
|---|---|---|
|**value**|object|Value of the property. Accepted values are: string, number, and boolean.|

_**Response**_

If successful, this method returns an empty response body.

---

#### Remove Model Property (DELETE)
[Top](#markdown-header-table-of-contents)

Deletes a single model property in a model’s property collection.

_**Request**_

```
DELETE /models/{code}/properties/{key}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|Code identifying the model.|
|**key**|string|Key of the model's property.|

_**Request Body**_

There is no request body.

_**Response**_

If successful, this method returns an empty response body.

## Model Device Property Template
[Top](#markdown-header-table-of-contents)

Device Property Templates is a way of defining properties that should be added and initialized on the device for a given model. Given a device property template with a key of “RadioType” and defaultValue of “Cellular” all devices created from the model will have a RadioType property added with value of “Cellular”. After device creation, a device’s properties can be updated independently.

### Representation

```json
{
    "key": string,
    "defaultValue": object
}
```
|Name|Type|Description|
|---|---|---|
|**key**|string|Identifies the device property template.|
|**defaultValue**|object|Default value of the device property template.|

### Methods
[Top](#markdown-header-table-of-contents)

* [Add Device Property Template (POST)](#markdown-header-add-device-property-template-post) - adds a single device property template to a model’s device property template collection.
* [Update Device Property Template (PUT)](#markdown-header-update-device-property-template-put) - updates a single device property template to a model’s device property template collection.
* [Remove Device Property Template (DELETE)](#markdown-header-remove-device-property-template-delete) - removes a single device property template to a model’s device property template collection.

---
#### Add Device Property Template (POST)
[Top](#markdown-header-table-of-contents)

Adds a single device property template to a model’s device property template collection. CAUTION: Invoking this API with an existing key will overwrite that property’s value, updating it. TODO: CHANGE IT SO THIS DOESN”T HAPPEN.

_**Request**_

```
POST /models/{code}/devicepropertytemplates
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|Code identifying the model.|

_**Request Body**_

```json
{
  "key": string,
  "defaultValue": object
}
```

|Name|Type|Description|
|---|---|---|
|**key**|string|Identifies the device property template.|
|**defaultValue**|object|Value of the device property template. Accepted values are: string, number, and boolean.|

_**Response**_

If successful, this method returns an empty response body.

---

#### Update Device Property Template (PUT)
[Top](#markdown-header-table-of-contents)

Updates a single model property in the model’s property collection.

_**Request**_

```
PUT /models/{code}/devicepropertytemplates/{key}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|Code identifying the model.|
|**key**|string|Key of the device property template.|

_**Request Body**_

```json
{
    "defualtValue": object
}
```

|Name|Type|Description|
|---|---|---|
|**defaultValue**|object|Default value of the device property template. Accepted values are: string, number, and boolean.|

_**Response**_

If successful, this method returns an empty response body.

---

#### Remove Device Property Template (DELETE)
[Top](#markdown-header-table-of-contents)

Deletes a single device property template in a model’s device property template collection.

_**Request**_

```
DELETE /models/{code}/properties/{key}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**code**|string|Code identifying the model.|
|**key**|string|Key of the device property template.|

_**Request Body**_

There is no request body.

_**Response**_

If successful, this method returns an empty response body.

## Device

## Device Status

## Device Property

---
DO NOT EDIT BELOW THIS LINE
---
---

## Resource Name
[Top](#markdown-header-table-of-contents)

Description of the resource.

### Representation

```json
{
    "string_prop": "value",
    "number_prop": 1,
    "bool_prop": true,
    "obj_prop": {
        "string_prop": "value"
    },
    "array_prop": [
        "value", 1, true
    ]
}
```
|Name|Type|Description|
|---|---|---|
|**stringprop**|string|This is a description for stringprop|

### Methods
[Top](#markdown-header-table-of-contents)

* [List Resources (GET)](#markdown-header-list-resources-get) - returns a paginated list of resources.
* [Single Resource (GET)](#markdown-header-single-resource-get) - returns a single resource.
* [Create Resource (POST)](#markdown-header-create-resource-post) - creates a single resource.
* [Update Resource (PUT)](#markdown-header-update-resource-put) - updates a single resource.
* [Remove Resource (DELETE)](#markdown-header-remove-resource-delete) - deletes a single resource.

---

#### List Resources (GET)
[Top](#markdown-header-table-of-contents)

Description of list of resources...

_**Request**_

```
GET /resources
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Headers**_|
|**header**|string|This is a description of the header.|
|_**Path**_|
|**path_prop**|string|This is a description for path_prop|
|_**Querystring**_|
|**querystring_prop**|string|This is a description for querystring_prop|

_**Request Body**_

There is no request body.

_**Response**_

If successful, this method will return a response body with the following structure.

```json
{
    "": "",
    "": ""
}
```

---

#### Single Resource (GET)
[Top](#markdown-header-table-of-contents)

Description of getting a single resource...

_**Request**_

```
GET /resources/{id}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**id**|string|The identifier of the resource.|

_**Request Body**_

There is no request body.

_**Response**_

If successful, this method will return a response body with the following structure.

```json
{
    "": "",
    "": ""
}
```

---

#### Create Resource (POST)
[Top](#markdown-header-table-of-contents)

Description of creating a resource...

_**Request**_

```
POST /resources/
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Headers**_|
|**header**|string|This is a description of the header.|
|_**Path**_|
|**pathprop**|string|This is a description for path_prop|
|_**Querystring**_|
|**querystringprop**|string|This is a description for querystring_prop|

_**Request Body**_

```json
{
    "": "",
    "": ""
}
```

|Name|Type|Description|
|---|---|---|
|**stringprop**|string|This is a description for string_prop|

_**Response**_

If successful, this method returns a status of 201 Created and a response body with the following structure.

```json
{
    "": "",
    "": ""
}
```

---

#### Update Resource (PUT)
[Top](#markdown-header-table-of-contents)

Description of updating a resource...

_**Request**_

```
PUT /resources/{id}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|id|string|The identifier of the resource|

_**Request Body**_

```json
{
    "": "",
    "": ""
}
```

|Name|Type|Description|
|---|---|---|
|**stringprop**|string|This is a description for string_prop|

_**Response**_

If successful, this method will return a response body with the following structure.

```json
{
    "": "",
    "": ""
}
```

---

#### Remove Resource (DELETE)
[Top](#markdown-header-table-of-contents)

Description of removing a resource...

_**Request**_

```
DELETE /resources/{id}
```

_**Parameters**_

|Name|Type|Description|
|---|---|---|
|_**Path**_|
|**id**|string|The identifier of the resource|

_**Request Body**_

There is no request body.

_**Response**_

If successful, will not return a response body.